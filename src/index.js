import React, {Component, PropTypes} from 'react';
import { render } from 'react-dom';
import App from 'components/app';

render (<App />, document.getElementById('app'));
