import React, {Component, PropTypes} from 'react';

export default class TableRow extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isEditing: false
		}
	}

	renderEditTableRow() {
		return (
			<form className="table-row-wrapper" onSubmit={this.onSaveClick.bind(this)}>
				<div>{this.props.id}</div>
				<input type="text" defaultValue={this.props.teamA} ref="TeamA" />
				<input type="text" defaultValue={this.props.teamB} ref="TeamB" />
				<div>
					<button type="submit" onClick={this.onSaveClick.bind(this)}>Save</button>
					<button type="button" onClick={this.onCancelClick.bind(this)}>Cancel</button>
				</div>
			</form>
		);
	}
		
	renderNormalTableRow() {
		return (
			<div className="table-row-wrapper">
				<div>{this.props.id}</div>
				<div>{this.props.teamA}</div>
				<div>{this.props.teamB}</div>
				<div>
					<button onClick={this.onEditClick.bind(this)}>Edit</button>
					<button onClick={this.onDeleteClick.bind(this)}>Delete</button>
				</div>
			</div>
		);
	}

	render() {

		if (this.state.isEditing) {

			return (
				<div className="table-row">
					{this.renderEditTableRow()}
				</div>
			);

		} else {

			return (
				<div className="table-row">	
					{this.renderNormalTableRow()}
				</div>
			);

		}

	}

	onEditClick() {
		this.setState({ isEditing: true });
	}

	onCancelClick() {
		this.setState({ isEditing: false });
	}

	onSaveClick(e) {
		e.preventDefault();

		var thisId = this.props.id;
		var newTeamA = this.refs.TeamA.value;
		var newTeamB = this.refs.TeamB.value;

		var editedRow = {
			thisId: thisId,
			newTeamA: newTeamA,
			newTeamB: newTeamB
		}

		this.props.editRow(editedRow);

		this.setState({ isEditing: false });
	}

	onDeleteClick() {
		var thisId = this.props.id;
		
		this.props.deleteRow(thisId);
	}
} 

