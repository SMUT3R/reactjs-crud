import React, {Component, PropTypes} from 'react';
import _ from 'lodash';
import Table from './table.js';
import CreateRow from './create-row.js';

const tableRows = [
{
	id: '1',
	teamA: 'Our Team',
	teamB: 'Their Team'
},
{
	id: '2',
	teamA: 'Best Team',
	teamB: 'Worst Team'
}
]

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			tableRows
		}
	}

	render() {
		return (
			<div className="root">
				<h1>CRUD App</h1>
				<Table 
					tableRows={this.state.tableRows} 
					editRow={this.editRow.bind(this)} 
					deleteRow={this.deleteRow.bind(this)} 
				/>
				<CreateRow createRow={this.createRow.bind(this)} />
			</div>
		);
	}

	createRow(createdRow) {

		var id = createdRow.valId;
		var teamA = createdRow.valTeamA;
		var teamB = createdRow.valTeamB;
		
		var tableRows = this.state.tableRows.concat({ id, teamA, teamB });
		
		this.setState({ tableRows });

	}

	editRow(editedRow) {
		var foundRow = _.find(this.state.tableRows, data => data.id === editedRow.thisId);

		foundRow.teamA = editedRow.newTeamA;
		foundRow.teamB = editedRow.newTeamB;

		this.setState({ tableRows: this.state.tableRows });
	}

	deleteRow(thisId) {
		_.remove(this.state.tableRows, data => data.id === thisId);

		this.setState({ tableRows: this.state.tableRows });
	}
}