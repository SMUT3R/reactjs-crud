import React, {Component, PropTypes} from 'react';

export default class CreateRow extends Component {

	render() {
		return (
			<form className="create-form" onSubmit={this.handleCreate.bind(this)}>
				<input id="createId" type="text" placeholder="ID" />
				<input id="TeamA" type="text" placeholder="Team A" />
				<input id="TeamB" type="text" placeholder="Team B" />
				<button type="submit">Create</button>
			</form>
		);
	}

	handleCreate(e) {
		e.preventDefault();

		var valId = document.getElementById("createId").value;
		var valTeamA = document.getElementById("TeamA").value;
		var valTeamB = document.getElementById("TeamB").value;

		var createdRow = {
			valId: valId,
			valTeamA: valTeamA,
			valTeamB: valTeamB
		}

		this.props.createRow(createdRow);

	  	var input = document.querySelectorAll('form.create-form input');

	  	for(var i = 0; i < input.length; i++) {
	    	  input[i].value = '';
	  	}
	}
}
