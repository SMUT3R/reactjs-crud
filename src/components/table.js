import React, {Component, PropTypes} from 'react';
import _ from 'lodash';
import TableHeader from './table-header.js';
import TableRow from './table-row.js';

export default class Table extends Component {
	renderRows() {
		const props = _.omit(this.props, 'tableRows');

		return _.map(this.props.tableRows, (data, index) => <TableRow key={index} {...data} {...props} />);
	}

	render() {
		return (
			<div className="table">
				<TableHeader />
				{this.renderRows()}
			</div>
		);
	}
} 
